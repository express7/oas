module.exports = async (oas, options) => {
  const { paths } = oas

  for (const pKey in paths) {
    const path = paths[pKey]

    for (const vKey in path) {
      const verb = path[vKey]
      const { parameters, requestBody } = verb

      if (parameters) {
        parameters.forEach((parameter, idx ) => {
          const { description } = parameter
          const match = description ? description.match(/ ex.*=.*/g) : undefined
          const ex = Array.isArray(match) ? match.shift() : undefined
          const example = ex ? ex.split('=').pop() : undefined

          if (example) {
            oas.paths[pKey][vKey].parameters[idx].description = description.replace(ex, '')
            oas.paths[pKey][vKey].parameters[idx].example = example
          }
        })
      }

      if (requestBody) {
        const { properties, example = {} } = requestBody.content['application/json'].schema
        
        for (const name in properties) {
          const { description } = properties[name]
          const match = description ? description.match(/ ex.*=.*/g) : undefined
          const ex = Array.isArray(match) ? match.shift() : undefined
          const value = ex ? ex.split('=').pop() : undefined

          oas.paths[pKey][vKey].requestBody.content['application/json'].schema.properties[name].description = description.replace(ex, '')

          example[name] = example[name] ? example[name] : value
       }
       
       oas.paths[pKey][vKey].requestBody.content['application/json'].schema.example = example
      }
    }
  }
  
  return oas
}
