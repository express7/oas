'use strict'

const debug = require('debug')('app:oas:paths')
const _ = require('app/helpers')
const { getRoutes } = require('app/router')

const getParameters = (url, method, options) => options.path.split(/[^\w\:\?]/gi)
  .filter(c => c.startsWith(':'))
  .filter(c => c !== ':model')
  .map(c => ({
    name: c.replace(/\W/gi, ''),
    in: 'path',
    type: 'string',
    required: !(c.indexOf('?') > 0),
    description: _.startCase(c)
  }))

const getSummary = (url, method, options) => {
  const methods = {
    get: 'List of {model}', // description: `Returns list of ${tag}.` }),
    post: 'Create {model}', // description: `Add a new ${tag} to collection.` })
    put: 'Update {model}',
    delete: 'Delete {model}',
    getid: 'Get a {model} by id', // description: `Returns a single ${tag}.` }),
    putid: 'Update a {model}', // description: `Update an existing ${tag}.` }),
    deleteid: 'Delete a {model}' // description: `Delete an existing ${tag}.` })
  }

  let result = _.compact(['', ...getParameters(url, method, options).map(c => c.name)].map(c => methods[method + c])).pop()  
  if (options.tags) result = result.replace(/\{model\}/gi, options.tags[0])

  return result
}

const getDescription = (url, method, options) => {
  const methods = {
    get: 'Returns list of {model}.',
    post: 'Add a new {model} to collection.',
    put: 'Update {model}',
    delete: 'Delete {model}',
    getid: 'Returns a single {model}.',
    putid: 'Update an existing {model}.',
    deleteid: 'Delete an existing {model}.'
  }

  let result = _.compact(['', ...getParameters(url, method, options).map(c => c.name)].map(c => methods[method + c])).pop()  
  if (options.tags) result = result.replace(/\{model\}/gi, options.tags[0])

  return result
}

const getInfo = (url, method, options = {}) => ({
  summary: getSummary(url, method, options),
  description: getDescription(url, method, options),
  tags: (options.tags || _.compact(options.rpath.split('/').map(c => _.kebabCase(c)))).filter(c => c !== 'api'),
  consumes: ['application/json'],
  produces: ['application/json'],
  parameters: getParameters(url, method, options),
  responses: { '200': { description: 'Successful response.' } }  
})

module.exports = (router, Models) => {
  return Object.entries(getRoutes(router)).reduce((a, [path, m]) => {
    if (path === '/swagger.json') return a

    const url = path.replace(/(\:.*?)[\W\/]|(\:.*?)$/gi, p => {
      return p.replace(':', '{') // remove double colon
        .replace(/\W$/gi, n => ('}' + n)) // replace non words
        .replace(/\w$/gi, n => (n + '}')) // add trailing }
    }).replace(/\?/gi, '') // remove ?

    const keys = path.indexOf(':model') > -1 ? Models : { undefined: '' }
    for (const key in _.kebabCase(keys)) {
      const [u, tags] = !!_.castType(key) ? [url.replace(/\{model\}/gi, key), [key]] : [url]

      a[u] = Object.entries(m).reduce((a, [m, v]) => ({ ...a, [m]: getInfo(u, m, { ...v, tags }) }), {})
    }

    return a
  }, {})
}
