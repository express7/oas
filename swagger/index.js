'use strict'

const debug = require('debug')('app:oas')
const path = require('node:path')
const swaggerInline = require('swagger-inline')
const _ = require('app/helpers')
const schemas = require('./schema')
const config = require('../config')

// const Env = require('app/env')
// const host = Env.get('HOST')
// const port = Env.get('PORT')

// const server = {
  // url: eval(`\`${Env.get('APP_URL')}\``).replace(/\/$/gi, '') + '/{basePath}',
  // description: 'The development API server',
  // variables: {
    // "username": {
    //   "default": "demo",
    //   "description": "this value is assigned by the service provider, in this example `gigantic-server.com`"
    // },
    // port: {
    //   enum: [
    //     3000,
    //     8080
    //   ],
    //   default: Env.get('PORT')
    // },
    // basePath: {
    //   default: Config.get('oas.basePath', '').replace(/^\//gi, '')
    // }
  // }
// }

module.exports = (options = {}, app) => {
  debug('-> oas', options)

  const {
    swagger,
    files: dirs,
    allowedVerbs
  } = _.merge(config, options)
  
  // files with api doc
  const files = [...dirs, 'node_modules/app/**/*.js', 'node_modules/map/**/*.js']

  // paths
  const paths = require('./paths')(app, schemas)

  debug('-> docOas', files)
  return swaggerInline(files, {
    base: path.join(__dirname, '../template.yaml'),
    format: '.json',
    ignore: []
  })
    // .catch(e => console.log('error', e))
    .then(async (docOas) => {
      debug('docOas ->', docOas)

      const baseOas = _.merge(JSON.parse(docOas), swagger, { paths }, { components: { schemas } })

      baseOas.paths = Object.entries(baseOas.paths)
        .sort() // sort
        .reduce((a, [path, methods]) => {
          // allowed verbs get, post, etc..
          a[path] = allowedVerbs
            ? Object.entries(methods).reduce((a, [m, v]) => (allowedVerbs.includes(m) ? { ...a, [m]: v } : a), {})
            : methods

          return a
        }, {})

      baseOas.components.schemas = Object.entries(baseOas.components.schemas)
        .sort() // sort
        .reduce((a, [k, v]) => ({ ...a, [k]: v }), {}) // to object

      // put example values
      const oas = await require('./example')(baseOas)

      debug('oas ->', oas)
      return oas
    })
}
