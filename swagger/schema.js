'use strict'

const debug = require('debug')('app:oas:schema')

module.exports = Object.entries(require('app/model/models'))
  .filter((k, v) => k !== 'Log') // remove log
  .sort() // sort
  .reduce((a, [key, schema]) => {
    try {
      const properties = Object.entries(schema.props).reduce((props, [prop, type]) => {
        if (Array.isArray(type)) type = type[0]
        if (type.name) type = type.name
        if (type.specificType) type = type.specificType
        if (typeof type === 'object') type = Object.keys(type)[0]
        if (type === 'text') type = 'string'
        type = type.toLowerCase()

        props[prop] = { type }

        return props
      }, {})

      a[key] = { type: 'object', properties }
    } catch (err) {
      debug(err)
    }

    return a
  }, {})
