# oas

[![npm version](https://img.shields.io/npm/v/express-validator.svg)](https://git.immobisp.com/express/oas)
[![Build Status](https://img.shields.io/travis/express-validator/express-validator.svg)](https://git.immobisp.com/express/oas)
[![Coverage Status](https://img.shields.io/coveralls/express-validator/express-validator.svg)](https://git.immobisp.com/express/oas/-/tree/main)

An [express.js](https://github.com/visionmedia/express) middleware for
[OpenAPI Specification document](https://git.immobisp.com/express/oas).

- [Installation](#installation)
- [Documentation](#documentation)
- [Changelog](#changelog)
- [License](#license)

## Installation

```console
npm install git+https://git.immobisp.com/express/oas.git
```

Follow up by registering the middleware inside the config/middleware.js array:

```js
module.exports = [
  'oas'
]
```

Define configuration inside config/oas.js:

```js
module.exports = {
  enable: true,
  specUrl: '/swagger.json',
  virtualPath: '/docs',
  options: {}
}
```

View the website at: http://localhost:3000/docs

Also make sure that you have Node.js 8 or newer in order to use it.

## Documentation

Please refer to the documentation website on https://git.immobisp.com/express/oas/-/blob/master/README.md.

## Changelog

Check the [GitLab Releases page](https://git.immobisp.com/express/oas/-/blob/master/CHANGELOG.md).

## License

MIT License
