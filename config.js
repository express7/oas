'use strict'

const _ = require('app/helpers')
const { name, description, version } = require('root/package.json')

module.exports = {
  /*
  |--------------------------------------------------------------------------
  | OAS Information
  | Please use OAS 3 Spesification Docs
  | https://swagger.io/docs/specification/2-0/basic-structure/
  |--------------------------------------------------------------------------
  */

  enable: true,
  specUrl: '/swagger.json',
  virtualPath: '/api-docs',

  // GET, HEAD, POST, PUT, DELETE, CONNECT, OPTIONS, TRACE, PATCH
  allowedVerbs: ['get'],

  // Path to the API docs
  // Sample usage
  // apis: [
  //    'docs/**/*.yml',    // load recursive all .yml file in docs directory
  //    'docs/**/*.js',     // load recursive all .js file in docs directory
  // ]
  files: ['app/**/*.js'],

  swagger: {
    openapi: '3.0.0',
    'x-api-id': _.kebabCase(name),

    info: {
      version,
      title: _.upperFirst(name),
      description,

      termsOfService: 'http://swagger.io/terms/',

      contact: {
        name: 'Swagger API Team',
        email: 'apiteam@swagger.io',
        url: 'http://swagger.io'
      },

      license: {
        name: 'Apache 2.0',
        url: 'https://www.apache.org/licenses/LICENSE-2.0.html'
      }
    },

    // servers: [
    //   {
    //     url: 'http://localhost:3000'
    //   }
    // ],

    basePath: '/api',
  },

  // Example security definitions.
  securityDefinitions: {
    ApiKey: {
      description: 'ApiKey description',
      name: 'Authorization'
    },

    // OAuth2 configuration
    OAuth2: {
      authorizationUrl: 'https://example.com/oauth/authorize',
      tokenUrl: 'https://example.com/oauth/token',

      // define your scopes here
      // remove read, write and admin if not necessary
      scopes: {
        read: 'Grants read access (this is just sample)',
        write: 'Grants write access (this is just sample)',
        admin: 'Grants read and write access to administrative information (this is just sample)'
      }
    }
  },

  resourceParameters: {
    resource: {
      get: {
        finder: { in: 'query', description: 'Field to be searched', example: 'id' },
        search: { in: 'query', description: 'String to be searched', example: '10000' },
        sorter: { in: 'query', description: 'Field as sorted key', example: 'id' },
        order: { in: 'query', description: 'asc/desc output items ordered', example: 'desc' },
        page: { in: 'query', description: 'Output page', default: 1, example: 1 },
        limit: { in: 'query', description: 'Output items per page', default: 10, example: 5 }
      }
    },

    resourceId: {
      get: {}
    }
  }
}
