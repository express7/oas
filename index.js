'use strict'

const debug = require('debug')(`app:oas:router`)
const swaggerUi = require('swagger-ui-express')
const swagger = require('./swagger')

module.exports = (options = {}, app) => {
  const {
    virtualPath = '/api-docs',
    specUrl = '/swagger.json'
  } = options

  app
    .use(virtualPath.replace(/^([^/])/, '/$1'),
      swaggerUi.serve,

      async (req, res, next) => {
        req.swaggerDoc = await swagger(options, app)
        return next()
      },

      swaggerUi.setup()
    )

    // swagger.json
    .get(specUrl.replace(/^([^/])/, '/$1'), async (req, res) => {
      const oasDoc = await swagger(options, app)
      return res.send(JSON.stringify(oasDoc, null, 2))
    })
  
  return (req, res, next) => next()
}
